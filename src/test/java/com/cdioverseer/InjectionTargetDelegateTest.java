/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer;

import static org.junit.Assert.assertSame;

import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.InjectionTarget;

import org.easymock.EasyMock;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

import com.cdioverseer.stats.counter.StatsCounter;

public class InjectionTargetDelegateTest {

    private static final String TEST_STRING = "test string";

    private EasyMockSupport support;

    private InjectionTarget<String> wrappedInjectionTarget;

    private CreationalContext<String> context;

    private StatsCounter statsCounter;

    private InjectionTargetDelegate<String> testedObject;

    @Before
    public void init() {
        support = new EasyMockSupport();

        wrappedInjectionTarget = support.createMock(InjectionTarget.class);
        statsCounter = support.createMock(StatsCounter.class);
        context = support.createMock(CreationalContext.class);

        testedObject = new InjectionTargetDelegate<String>(wrappedInjectionTarget, statsCounter);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNullStatsCounter() {
        new InjectionTargetDelegate<String>(wrappedInjectionTarget, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNullInjectionTarget() {
        new InjectionTargetDelegate<String>(null, statsCounter);
    }

    @Test
    public void shouldDelegateInject() {
        wrappedInjectionTarget.inject(TEST_STRING, context);

        support.replayAll();

        testedObject.inject(TEST_STRING, context);

        support.verifyAll();
    }

    @Test
    public void shouldDelegatePostConstruct() {
        wrappedInjectionTarget.postConstruct(TEST_STRING);
        statsCounter.instanceConstructed(TEST_STRING.getClass());

        support.replayAll();

        testedObject.postConstruct(TEST_STRING);

        support.verifyAll();
    }

    @Test
    public void shouldDelegatePreDestroyed() {
        wrappedInjectionTarget.preDestroy(TEST_STRING);
        statsCounter.instanceDestroyed(TEST_STRING.getClass());

        support.replayAll();

        testedObject.preDestroy(TEST_STRING);

        support.verifyAll();
    }

    @Test
    public void shouldDelegateProduce() {
        EasyMock.expect(wrappedInjectionTarget.produce(context)).andReturn(TEST_STRING);

        support.replayAll();

        final String result = testedObject.produce(context);

        support.verifyAll();

        assertSame("Should return object produced by wrapper", TEST_STRING, result);
    }

    @Test
    public void shouldDelegateDispose() {
        wrappedInjectionTarget.dispose(TEST_STRING);

        support.replayAll();

        testedObject.dispose(TEST_STRING);

        support.verifyAll();
    }

    @Test
    public void shouldDelegateGetInjectionPoints() {
        final Set<InjectionPoint> iPoints = support.createMock(Set.class);
        EasyMock.expect(wrappedInjectionTarget.getInjectionPoints()).andReturn(iPoints);

        support.replayAll();

        final Set<InjectionPoint> result = testedObject.getInjectionPoints();

        support.verifyAll();

        assertSame("Should return points from wrapper", iPoints, result);
    }
}
