/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.enterprise.inject.spi.ProcessInjectionTarget;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

import com.cdioverseer.query.OrderBy;
import com.cdioverseer.query.Ordering;
import com.cdioverseer.query.Query;
import com.cdioverseer.stats.StatsEntry;

public class OverseerTest {

    private static class TestClassA {
    }

    private static class TestClassB {
    }

    private static class TestClassC {
    }

    private static class TestClassD {
    }

    private static final Class<TestClassA> TOKEN_A = TestClassA.class;

    private static final Class<TestClassB> TOKEN_B = TestClassB.class;

    private static final Class<TestClassC> TOKEN_C = TestClassC.class;

    private static final Class<TestClassD> TOKEN_D = TestClassD.class;

    private static final int TEST_A_CONSTRUCT = 3333;

    private static final int TEST_B_CONSTRUCT = 2222;

    private static final int TEST_C_CONSTRUCT = 4444;

    private static final int TEST_D_CONSTRUCT = 6666;

    private static final int TEST_A_DESTROY = 333;

    private static final int TEST_B_DESTROY = 222;

    private static final int TEST_C_DESTROY = 444;

    private static final int TEST_D_DESTROY = 666;

    private EasyMockSupport support;

    private ProcessInjectionTarget<TestClassA> processInjectionTargetA;

    private ProcessInjectionTarget<TestClassB> processInjectionTargetB;

    private ProcessInjectionTarget<TestClassC> processInjectionTargetC;

    private ProcessInjectionTarget<TestClassD> processInjectionTargetD;

    private InjectionTarget<TestClassA> injectionTargetA;

    private InjectionTarget<TestClassB> injectionTargetB;

    private InjectionTarget<TestClassC> injectionTargetC;

    private InjectionTarget<TestClassD> injectionTargetD;

    private Capture<InjectionTarget<TestClassA>> delegateCaptureA;

    private Capture<InjectionTarget<TestClassB>> delegateCaptureB;

    private Capture<InjectionTarget<TestClassC>> delegateCaptureC;

    private Capture<InjectionTarget<TestClassD>> delegateCaptureD;

    private Overseer testedObject;

    @Before
    public void init() {
        support = new EasyMockSupport();

        testedObject = new Overseer();

        delegateCaptureA = new Capture<InjectionTarget<TestClassA>>();
        delegateCaptureB = new Capture<InjectionTarget<TestClassB>>();
        delegateCaptureC = new Capture<InjectionTarget<TestClassC>>();
        delegateCaptureD = new Capture<InjectionTarget<TestClassD>>();
        injectionTargetA = support.createMock(InjectionTarget.class);
        injectionTargetB = support.createMock(InjectionTarget.class);
        injectionTargetC = support.createMock(InjectionTarget.class);
        injectionTargetD = support.createMock(InjectionTarget.class);
        processInjectionTargetA = support.createMock(ProcessInjectionTarget.class);
        processInjectionTargetB = support.createMock(ProcessInjectionTarget.class);
        processInjectionTargetC = support.createMock(ProcessInjectionTarget.class);
        processInjectionTargetD = support.createMock(ProcessInjectionTarget.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNullQuery() {
        support.replayAll();

        try {
            testedObject.getQueryResult(null);
        } finally {
            support.verifyAll();
        }
    }

    @Test
    public void shouldSortByClassAndFilterResults() {
        prepareInjectionTargets();
        expectSampleData();

        support.replayAll();

        constructSampleData();

        final Query q = Query.createBuilder().withOrderBy(OrderBy.CLASS_NAME).withOrdering(Ordering.ASC).withStart(1)
                .withCount(2).build();
        final List<StatsEntry> result = testedObject.getQueryResult(q);

        support.verifyAll();

        assertNotNull("Should return not null result", result);
        assertEquals("Result should have two elements", 2, result.size());
        final StatsEntry entry1 = result.get(0);
        final StatsEntry entry2 = result.get(1);
        assertEquals("First return should have proper class", TOKEN_B.getName(), entry1.getClassName());
        assertEquals("First return should have proper constructedCount", TEST_B_CONSTRUCT, entry1.getConstructedCount());
        assertEquals("First return should have proper destroyedCount", TEST_B_DESTROY, entry1.getDestroyedCount());
        assertEquals("Second return should have proper class", TOKEN_C.getName(), entry2.getClassName());
        assertEquals("Second return should have proper constructedCount", TEST_C_CONSTRUCT,
                entry2.getConstructedCount());
        assertEquals("Second return should have proper destroyedCount", TEST_C_DESTROY, entry2.getDestroyedCount());
    }

    @Test
    public void shouldSortByConstructCountAndFilterResults() {
        prepareInjectionTargets();
        expectSampleData();

        support.replayAll();

        constructSampleData();

        final Query q = Query.createBuilder().withOrderBy(OrderBy.CONSTRUCTED_COUNT).withOrdering(Ordering.DESC)
                .withStart(2).withCount(2).build();
        final List<StatsEntry> result = testedObject.getQueryResult(q);

        support.verifyAll();

        assertNotNull("Should return not null result", result);
        assertEquals("Result should have two elements", 2, result.size());
        final StatsEntry entry1 = result.get(0);
        final StatsEntry entry2 = result.get(1);
        assertEquals("First return should have proper class", TOKEN_A.getName(), entry1.getClassName());
        assertEquals("First return should have proper constructedCount", TEST_A_CONSTRUCT, entry1.getConstructedCount());
        assertEquals("First return should have proper destroyedCount", TEST_A_DESTROY, entry1.getDestroyedCount());
        assertEquals("Second return should have proper class", TOKEN_B.getName(), entry2.getClassName());
        assertEquals("Second return should have proper constructedCount", TEST_B_CONSTRUCT,
                entry2.getConstructedCount());
        assertEquals("Second return should have proper destroyedCount", TEST_B_DESTROY, entry2.getDestroyedCount());
    }

    @Test
    public void shouldSortByDestroyCountAndFilterResults() {
        prepareInjectionTargets();
        expectSampleData();

        support.replayAll();

        constructSampleData();

        final Query q = Query.createBuilder().withOrderBy(OrderBy.DESTROYED_COUNT).withOrdering(Ordering.DESC)
                .withStart(3).withCount(100).build();
        final List<StatsEntry> result = testedObject.getQueryResult(q);

        support.verifyAll();

        assertNotNull("Should return not null result", result);
        assertEquals("Result should have one element", 1, result.size());
        final StatsEntry entry = result.get(0);
        assertEquals("Element should have proper class", TOKEN_B.getName(), entry.getClassName());
        assertEquals("Element should have proper constructedCount", TEST_B_CONSTRUCT, entry.getConstructedCount());
        assertEquals("Element should have proper destroyedCount", TEST_B_DESTROY, entry.getDestroyedCount());
    }

    @Test
    public void shouldReturnEmptyResultsOnNonOverlapingQuery() {
        prepareInjectionTargets();
        expectSampleData();

        support.replayAll();

        constructSampleData();

        final Query q = Query.createBuilder().withOrderBy(OrderBy.CLASS_NAME).withOrdering(Ordering.ASC).withStart(10)
                .withCount(2).build();
        final List<StatsEntry> result = testedObject.getQueryResult(q);

        support.verifyAll();

        assertNotNull("Should return not null result", result);
        assertTrue("Result should be empty", result.isEmpty());
    }

    @Test
    public void shouldReturnEmptyResultsAfterClear() {
        prepareInjectionTargets();
        expectSampleData();

        support.replayAll();

        constructSampleData();
        testedObject.clearStats();

        final Query q = Query.createBuilder().withOrderBy(OrderBy.CLASS_NAME).withOrdering(Ordering.ASC).withStart(0)
                .withCount(1000).build();
        final List<StatsEntry> result = testedObject.getQueryResult(q);

        support.verifyAll();

        assertNotNull("Should return not null result", result);
        assertTrue("Result should be empty", result.isEmpty());
    }

    @Test
    public void shouldReturnZeroDestructionsWhenClassNotDestroyed() {
        prepareInjectionTarget(processInjectionTargetA, injectionTargetA, delegateCaptureA);
        expectSampleData(injectionTargetA, TOKEN_A, TEST_A_CONSTRUCT, 0);

        support.replayAll();

        constructSampleData(testedObject, new TestClassA(), processInjectionTargetA, delegateCaptureA,
                TEST_A_CONSTRUCT, 0);

        final Query q = Query.createBuilder().withOrderBy(OrderBy.CLASS_NAME).withOrdering(Ordering.ASC).withStart(0)
                .withCount(2).build();
        final List<StatsEntry> result = testedObject.getQueryResult(q);

        support.verifyAll();

        assertNotNull("Should return not null result", result);
        assertEquals("Result should have one entry", 1, result.size());
        final StatsEntry resultEntry = result.get(0);
        assertEquals("Element should have proper class", TOKEN_A.getName(), resultEntry.getClassName());
        assertEquals("Element should have proper constructedCount", TEST_A_CONSTRUCT, resultEntry.getConstructedCount());
        assertEquals("Element should have proper destroyedCount", 0, resultEntry.getDestroyedCount());
    }

    private void prepareInjectionTargets() {
        prepareInjectionTarget(processInjectionTargetA, injectionTargetA, delegateCaptureA);
        prepareInjectionTarget(processInjectionTargetB, injectionTargetB, delegateCaptureB);
        prepareInjectionTarget(processInjectionTargetC, injectionTargetC, delegateCaptureC);
        prepareInjectionTarget(processInjectionTargetD, injectionTargetD, delegateCaptureD);
    }

    private <T> void prepareInjectionTarget(final ProcessInjectionTarget<T> processInjectionTarget,
            final InjectionTarget<T> injectionTarget, final Capture<InjectionTarget<T>> capture) {
        final AnnotatedType<T> annotatedType = support.createMock(AnnotatedType.class);
        EasyMock.expect(processInjectionTarget.getInjectionTarget()).andReturn(injectionTarget).anyTimes();
        EasyMock.expect(processInjectionTarget.getAnnotatedType()).andReturn(annotatedType).anyTimes();
        processInjectionTarget.setInjectionTarget(EasyMock.capture(capture));
    }

    private void expectSampleData() {
        expectSampleData(injectionTargetA, TOKEN_A, TEST_A_CONSTRUCT, TEST_A_DESTROY);
        expectSampleData(injectionTargetB, TOKEN_B, TEST_B_CONSTRUCT, TEST_B_DESTROY);
        expectSampleData(injectionTargetC, TOKEN_C, TEST_C_CONSTRUCT, TEST_C_DESTROY);
        expectSampleData(injectionTargetD, TOKEN_D, TEST_D_CONSTRUCT, TEST_D_DESTROY);
    }

    private <T> void expectSampleData(final InjectionTarget<T> injectionTarget, final Class<T> token,
            final int constructCount, final int destroyCount) {
        injectionTarget.postConstruct(EasyMock.anyObject(token));
        EasyMock.expectLastCall().times(constructCount);
        if (destroyCount > 0) {
            injectionTarget.preDestroy(EasyMock.anyObject(token));
            EasyMock.expectLastCall().times(destroyCount);
        }

    }

    private void constructSampleData() {
        constructSampleData(testedObject, new TestClassA(), processInjectionTargetA, delegateCaptureA,
                TEST_A_CONSTRUCT, TEST_A_DESTROY);
        constructSampleData(testedObject, new TestClassB(), processInjectionTargetB, delegateCaptureB,
                TEST_B_CONSTRUCT, TEST_B_DESTROY);
        constructSampleData(testedObject, new TestClassC(), processInjectionTargetC, delegateCaptureC,
                TEST_C_CONSTRUCT, TEST_C_DESTROY);
        constructSampleData(testedObject, new TestClassD(), processInjectionTargetD, delegateCaptureD,
                TEST_D_CONSTRUCT, TEST_D_DESTROY);
    }

    private <T> void constructSampleData(final Overseer o, final T instance,
            final ProcessInjectionTarget<T> processInjectionTarget, final Capture<InjectionTarget<T>> capture,
            final int constructCount, final int destroyCount) {
        o.processInjectionTarget(processInjectionTarget);
        final InjectionTarget<T> captured = capture.getValue();
        for (int i = 0; i < constructCount; ++i) {
            captured.postConstruct(instance);
        }
        for (int i = 0; i < destroyCount; ++i) {
            captured.preDestroy(instance);
        }
    }
}