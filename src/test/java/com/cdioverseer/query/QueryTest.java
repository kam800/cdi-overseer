/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.query;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class QueryTest {

    @Test
    public void shouldCreateRegularQuery() {
        final OrderBy testOrderBy = OrderBy.CLASS_NAME;
        final Ordering testOrdering = Ordering.ASC;
        final int testStart = 4;
        final int testCount = 11;

        final Query testQuery = Query.createBuilder().withOrderBy(testOrderBy).withOrdering(testOrdering)
                .withStart(testStart).withCount(testCount).build();

        assertEquals(testOrderBy, testQuery.getOrderBy());
        assertEquals(testOrdering, testQuery.getOrdering());
        assertEquals(testStart, testQuery.getStart());
        assertEquals(testCount, testQuery.getCount());
    }

    @Test
    public void shouldCreateZeroQuery() {
        final OrderBy testOrderBy = OrderBy.CLASS_NAME;
        final Ordering testOrdering = Ordering.ASC;
        final int testStart = 0;
        final int testCount = 0;

        final Query testQuery = Query.createBuilder().withOrderBy(testOrderBy).withOrdering(testOrdering)
                .withStart(testStart).withCount(testCount).build();

        assertEquals(testOrderBy, testQuery.getOrderBy());
        assertEquals(testOrdering, testQuery.getOrdering());
        assertEquals(testStart, testQuery.getStart());
        assertEquals(testCount, testQuery.getCount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNullOrderBy() {
        final Ordering testOrdering = Ordering.DESC;
        final int testStart = 20;
        final int testCount = 123;

        Query.createBuilder().withOrdering(testOrdering).withStart(testStart).withCount(testCount).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNullOrdering() {
        final OrderBy testOrderBy = OrderBy.CONSTRUCTED_COUNT;
        final int testStart = 23;
        final int testCount = 240;

        Query.createBuilder().withOrderBy(testOrderBy).withStart(testStart).withCount(testCount).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNegativeStart() {
        final OrderBy testOrderBy = OrderBy.CONSTRUCTED_COUNT;
        final Ordering testOrdering = Ordering.DESC;
        final int testStart = -2;
        final int testCount = 10;

        Query.createBuilder().withOrderBy(testOrderBy).withOrdering(testOrdering).withStart(testStart)
                .withCount(testCount).build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNegativeCount() {
        final OrderBy testOrderBy = OrderBy.CONSTRUCTED_COUNT;
        final Ordering testOrdering = Ordering.DESC;
        final int testStart = 84;
        final int testCount = -2;

        Query.createBuilder().withOrderBy(testOrderBy).withOrdering(testOrdering).withStart(testStart)
                .withCount(testCount).build();
    }
}
