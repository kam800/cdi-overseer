/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import com.cdioverseer.query.OrderBy;
import com.cdioverseer.query.Ordering;
import com.cdioverseer.query.Query;
import com.cdioverseer.stats.StatsEntry;

public class ComparatorBuilderTest {

    private static final StatsEntry ENTRY_1 = new StatsEntry("class1", 10, 500);

    private static final StatsEntry ENTRY_2 = new StatsEntry("class2", 1000, 5);

    private static final StatsEntry ENTRY_3 = new StatsEntry("class3", 1, 50);

    private static final StatsEntry ENTRY_4 = new StatsEntry("class4", 100, 5000);

    @Test
    public void shouldBuildClassNameAscendingComparator() {
        final Comparator<StatsEntry> result = ComparatorBuilder.createBuilder().withOrderBy(OrderBy.CLASS_NAME)
                .withOrdering(Ordering.ASC).build();
        final List<StatsEntry> entries = asList(ENTRY_4, ENTRY_2, ENTRY_1, ENTRY_3);

        Collections.sort(entries, result);

        final List<StatsEntry> expectedEntries = asList(ENTRY_1, ENTRY_2, ENTRY_3, ENTRY_4);
        assertEquals("Should sort entries in proper order", expectedEntries, entries);
    }

    @Test
    public void shouldBuildClassNameDescendingComparator() {
        final Comparator<StatsEntry> result = ComparatorBuilder.createBuilder().withOrderBy(OrderBy.CLASS_NAME)
                .withOrdering(Ordering.DESC).build();
        final List<StatsEntry> entries = asList(ENTRY_4, ENTRY_2, ENTRY_1, ENTRY_3);

        Collections.sort(entries, result);

        final List<StatsEntry> expectedEntries = asList(ENTRY_4, ENTRY_3, ENTRY_2, ENTRY_1);
        assertEquals("Should sort entries in proper order", expectedEntries, entries);
    }

    @Test
    public void shouldBuildContructedCountAscendingComparator() {
        final Comparator<StatsEntry> result = ComparatorBuilder.createBuilder().withOrderBy(OrderBy.CONSTRUCTED_COUNT)
                .withOrdering(Ordering.ASC).build();
        final List<StatsEntry> entries = asList(ENTRY_1, ENTRY_2, ENTRY_3, ENTRY_4);

        Collections.sort(entries, result);

        final List<StatsEntry> expectedEntries = asList(ENTRY_3, ENTRY_1, ENTRY_4, ENTRY_2);
        assertEquals("Should sort entries in proper order", expectedEntries, entries);
    }

    @Test
    public void shouldBuildContructedCountDescendingComparator() {
        final Comparator<StatsEntry> result = ComparatorBuilder.createBuilder().withOrderBy(OrderBy.CONSTRUCTED_COUNT)
                .withOrdering(Ordering.DESC).build();
        final List<StatsEntry> entries = asList(ENTRY_1, ENTRY_2, ENTRY_3, ENTRY_4);

        Collections.sort(entries, result);

        final List<StatsEntry> expectedEntries = asList(ENTRY_2, ENTRY_4, ENTRY_1, ENTRY_3);
        assertEquals("Should sort entries in proper order", expectedEntries, entries);
    }

    @Test
    public void shouldBuildDestroyedCountAscendingComparator() {
        final Comparator<StatsEntry> result = ComparatorBuilder.createBuilder().withOrderBy(OrderBy.DESTROYED_COUNT)
                .withOrdering(Ordering.ASC).build();
        final List<StatsEntry> entries = asList(ENTRY_1, ENTRY_2, ENTRY_3, ENTRY_4);

        Collections.sort(entries, result);

        final List<StatsEntry> expectedEntries = asList(ENTRY_2, ENTRY_3, ENTRY_1, ENTRY_4);
        assertEquals("Should sort entries in proper order", expectedEntries, entries);
    }

    @Test
    public void shouldBuildDestroyedCountDescendingComparator() {
        final Comparator<StatsEntry> result = ComparatorBuilder.createBuilder().withOrderBy(OrderBy.DESTROYED_COUNT)
                .withOrdering(Ordering.DESC).build();
        final List<StatsEntry> entries = asList(ENTRY_1, ENTRY_2, ENTRY_3, ENTRY_4);

        Collections.sort(entries, result);

        final List<StatsEntry> expectedEntries = asList(ENTRY_4, ENTRY_1, ENTRY_3, ENTRY_2);
        assertEquals("Should sort entries in proper order", expectedEntries, entries);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldComplainOnNullOrderBy() {
        ComparatorBuilder.createBuilder().withOrderBy(null).withOrdering(Ordering.ASC).build();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldComplainOnNullOrdering() {
        ComparatorBuilder.createBuilder().withOrderBy(OrderBy.CLASS_NAME).withOrdering(null).build();
    }

    @Test
    public void shouldBuildComparatorWithQuery() {
        final Query q = Query.createBuilder().withOrderBy(OrderBy.CONSTRUCTED_COUNT).withOrdering(Ordering.DESC)
                .build();
        final Comparator<StatsEntry> result = ComparatorBuilder.createBuilder().withQuery(q).build();
        final List<StatsEntry> entries = asList(ENTRY_1, ENTRY_2, ENTRY_3, ENTRY_4);

        Collections.sort(entries, result);

        final List<StatsEntry> expectedEntries = asList(ENTRY_2, ENTRY_4, ENTRY_1, ENTRY_3);
        assertEquals("Should sort entries in proper order", expectedEntries, entries);
    }

    private <T> List<T> asList(final T... elements) {
        final ArrayList<T> result = new ArrayList<T>(elements.length);
        for (final T e : elements) {
            result.add(e);
        }
        return result;
    }
}
