/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Comparator;

import org.easymock.EasyMock;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

import com.cdioverseer.stats.StatsEntry;

public class StatsEntryConstructedCountComparatorTest {

    private static final int EQUALS_COMPARATOR_RESULT = 0;

    private static final int TEST_CLASS_CONSTRUCTED_1 = 73;

    private static final int TEST_CLASS_CONSTRUCTED_2 = 102;

    private static final int TEST_CLASS_CONSTRUCTED_3 = 1289;

    private EasyMockSupport support;

    private Comparator<StatsEntry> testedObject;

    @Before
    public void init() {
        support = new EasyMockSupport();

        testedObject = new StatsEntryConstructedCountComparator();
    }

    @Test
    public void shouldBeReflexive() {
        final StatsEntry entry1 = support.createMock(StatsEntry.class);
        EasyMock.expect(entry1.getConstructedCount()).andReturn(TEST_CLASS_CONSTRUCTED_1).anyTimes();

        support.replayAll();
        final int compareResult = testedObject.compare(entry1, entry1);
        support.verifyAll();

        assertEquals(EQUALS_COMPARATOR_RESULT, compareResult);
    }

    @Test
    public void shouldBeAntiSymmetric() {
        final StatsEntry entry1 = support.createMock(StatsEntry.class);
        final StatsEntry entry2 = support.createMock(StatsEntry.class);
        EasyMock.expect(entry1.getConstructedCount()).andReturn(TEST_CLASS_CONSTRUCTED_1).anyTimes();
        EasyMock.expect(entry2.getConstructedCount()).andReturn(TEST_CLASS_CONSTRUCTED_2).anyTimes();

        support.replayAll();
        final int compareOne2TwoResult = testedObject.compare(entry1, entry2);
        final int compareTwo2OneResult = testedObject.compare(entry2, entry1);
        support.verifyAll();

        assertTrue(compareOne2TwoResult < 0);
        assertTrue(compareTwo2OneResult > 0);
    }

    @Test
    public void shouldBeTransitive() {
        final StatsEntry entry1 = support.createMock(StatsEntry.class);
        final StatsEntry entry2 = support.createMock(StatsEntry.class);
        final StatsEntry entry3 = support.createMock(StatsEntry.class);
        EasyMock.expect(entry1.getConstructedCount()).andReturn(TEST_CLASS_CONSTRUCTED_1).anyTimes();
        EasyMock.expect(entry2.getConstructedCount()).andReturn(TEST_CLASS_CONSTRUCTED_2).anyTimes();
        EasyMock.expect(entry3.getConstructedCount()).andReturn(TEST_CLASS_CONSTRUCTED_3).anyTimes();

        support.replayAll();
        final int compareOne2TwoResult = testedObject.compare(entry1, entry2);
        final int compareTwo2ThreeResult = testedObject.compare(entry2, entry3);
        final int compareOne2ThreeResult = testedObject.compare(entry1, entry3);
        support.verifyAll();

        assertTrue(compareOne2TwoResult < 0);
        assertTrue(compareTwo2ThreeResult < 0);
        assertTrue(compareOne2ThreeResult < 0);
    }
}
