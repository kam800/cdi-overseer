/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.counter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MutableIntegerTest {

    private static final int DEFAULT_INT_VALUE = 9230;

    @Test
    public void shouldReturnInitialValue() {
        final MutableInteger mutableInteger = new MutableInteger(DEFAULT_INT_VALUE);

        final int result = mutableInteger.getValue();

        assertEquals("Should return proper value", DEFAULT_INT_VALUE, result);
    }

    @Test
    public void shouldReturnRecentlySetValue() {
        final MutableInteger mutableInteger = new MutableInteger(0);

        mutableInteger.setValue(DEFAULT_INT_VALUE);
        final int result = mutableInteger.getValue();

        assertEquals("Should return proper value", DEFAULT_INT_VALUE, result);
    }

    @Test
    public void shouldIncrementReturningNewValue() {
        final MutableInteger mutableInteger = new MutableInteger(DEFAULT_INT_VALUE);

        final int result = mutableInteger.incValue();

        assertEquals("Should return new value", DEFAULT_INT_VALUE + 1, result);
    }

    @Test
    public void shouldIncrementMutableInteger() {
        final MutableInteger mutableInteger = new MutableInteger(DEFAULT_INT_VALUE);

        mutableInteger.incValue();
        final int result = mutableInteger.getValue();

        assertEquals("Should return new value", DEFAULT_INT_VALUE + 1, result);
    }
}
