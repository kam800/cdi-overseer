/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.counter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.cdioverseer.stats.CdiEvent;

public class StatsCounterTest {

    private static final Integer INTEGER_1 = Integer.valueOf(1);

    private StatsCounter testedObject;

    @Before
    public void init() {
        testedObject = new StatsCounter();
    }

    @Test
    public void shouldInitiallyReturnEmptyConstructedMap() {
        final Map<Class<?>, Integer> constructedResult = testedObject.getConstructedCounts();
        assertTrue("Result should be empty", constructedResult.isEmpty());
    }

    @Test
    public void shouldInitiallyReturnEmptyDestroyedMap() {
        final Map<Class<?>, Integer> destoyedResult = testedObject.getDestroyedCounts();
        assertTrue("Result should be empty", destoyedResult.isEmpty());
    }

    @Test
    public void shouldInitiallyReturnEmptyEventList() {
        final List<CdiEvent> eventsResult = testedObject.getEventList();
        assertTrue("Result should be empty", eventsResult.isEmpty());
    }

    @Test
    public void shouldLogInstanceConstruction() {
        final Class<?> testClassToken = TestClass.class;
        testedObject.instanceConstructed(testClassToken);

        final List<CdiEvent> eventsResult = testedObject.getEventList();
        final Map<Class<?>, Integer> constructedResult = testedObject.getConstructedCounts();
        final Map<Class<?>, Integer> destroyedResult = testedObject.getDestroyedCounts();

        assertEquals("Events list should have proper number of elements", 1, eventsResult.size());
        assertEquals("Constructed map should have proper number of elements", 1, constructedResult.size());
        assertEquals("Destroyed map should have proper number of elements", 0, destroyedResult.size());

        final CdiEvent resultEvent = eventsResult.get(0);
        assertEquals("Event should have proper class token", testClassToken, resultEvent.getClassToken());

        final Integer resultCount = constructedResult.get(testClassToken);
        assertEquals("Construction map should contain proper count for the token", INTEGER_1, resultCount);
    }

    @Test
    public void shouldLogInstanceDestruction() {
        final Class<?> testClassToken = TestClass.class;
        testedObject.instanceDestroyed(testClassToken);

        final List<CdiEvent> eventsResult = testedObject.getEventList();
        final Map<Class<?>, Integer> constructedResult = testedObject.getConstructedCounts();
        final Map<Class<?>, Integer> destroyedResult = testedObject.getDestroyedCounts();

        assertEquals("Events list should have proper number of elements", 1, eventsResult.size());
        assertEquals("Constructed map should have proper number of elements", 0, constructedResult.size());
        assertEquals("Destroyed map should have proper number of elements", 1, destroyedResult.size());

        final CdiEvent resultEvent = eventsResult.get(0);
        assertEquals("Event should have proper class token", testClassToken, resultEvent.getClassToken());

        final Integer resultCount = destroyedResult.get(testClassToken);
        assertEquals("Destroy map should contain proper count for the token", INTEGER_1, resultCount);
    }

    @Test
    public void shouldReturnEmptyResultAfterClear() {
        final Class<?> testClassToken = TestClass.class;
        testedObject.instanceConstructed(testClassToken);
        testedObject.instanceDestroyed(testClassToken);

        final List<CdiEvent> eventsResult = testedObject.getEventList();
        final Map<Class<?>, Integer> constructedResult = testedObject.getConstructedCounts();
        final Map<Class<?>, Integer> destroyedResult = testedObject.getDestroyedCounts();
        testedObject.clear();
        final List<CdiEvent> secondEventsResult = testedObject.getEventList();
        final Map<Class<?>, Integer> secondConstructedResult = testedObject.getConstructedCounts();
        final Map<Class<?>, Integer> secondDestroyedResult = testedObject.getDestroyedCounts();

        assertEquals("Events list should have proper number of elements", 2, eventsResult.size());
        assertEquals("Constructed map should have proper number of elements", 1, constructedResult.size());
        assertEquals("Destroyed map should have proper number of elements", 1, destroyedResult.size());
        assertTrue("Events list should be empty after clear", secondEventsResult.isEmpty());
        assertTrue("Constructed map should be empty after clear", secondConstructedResult.isEmpty());
        assertTrue("Destroyed map should be empty after clear", secondDestroyedResult.isEmpty());

        testedObject.clear();
        final CdiEvent resultEvent = eventsResult.get(0);
        assertEquals("Event should have proper class token", testClassToken, resultEvent.getClassToken());

        final Integer resultCount = destroyedResult.get(testClassToken);
        assertEquals("Destroy map should contain proper count for the token", INTEGER_1, resultCount);
    }

    @Test
    public void shouldLogCoupleOfDestructionsAndDestructions() {
        final Class<?> firstClassToken = TestClass.class;
        final Class<?> secondClassToken = TestClass2.class;
        final int firstClassConstructionCount = 20;
        final int secondClassConstructionCount = 31;
        final int firstClassDestructionCount = 10;
        final int secondClassDestructionCount = 1;
        final int allEventsCount = firstClassConstructionCount + secondClassConstructionCount
                + firstClassDestructionCount + secondClassDestructionCount;

        final Map<Class<?>, Integer> expectedConstructedResult = new HashMap<Class<?>, Integer>();
        final Map<Class<?>, Integer> expectedDestroyedResult = new HashMap<Class<?>, Integer>();
        expectedConstructedResult.put(firstClassToken, firstClassConstructionCount);
        expectedConstructedResult.put(secondClassToken, secondClassConstructionCount);
        expectedDestroyedResult.put(firstClassToken, firstClassDestructionCount);
        expectedDestroyedResult.put(secondClassToken, secondClassDestructionCount);

        performConstructionsAndDestructions(firstClassToken, firstClassConstructionCount, firstClassDestructionCount);
        performConstructionsAndDestructions(secondClassToken, secondClassConstructionCount, secondClassDestructionCount);

        final List<CdiEvent> eventsResult = testedObject.getEventList();
        final Map<Class<?>, Integer> constructedResult = testedObject.getConstructedCounts();
        final Map<Class<?>, Integer> destroyedResult = testedObject.getDestroyedCounts();

        assertEquals("Events list should have proper number of elements", allEventsCount, eventsResult.size());
        assertEquals("Should return proper constructed map", expectedConstructedResult, constructedResult);
        assertEquals("Should return proper destroyed map", expectedDestroyedResult, destroyedResult);
    }

    @Test
    public void shouldFillWholeEventsListAndRemoveFirstElement() {
        final Class<?> testClassToken = TestClass.class;
        performConstructionsAndDestructions(testClassToken, StatsCounter.EVENTS_HISTORY_LENGTH, 0);

        final List<CdiEvent> eventsResult = testedObject.getEventList();
        final Integer constructedResult = testedObject.getConstructedCounts().get(testClassToken);
        testedObject.instanceConstructed(testClassToken);
        final List<CdiEvent> secondEventsResult = testedObject.getEventList();
        final Integer secondConstructedResult = testedObject.getConstructedCounts().get(testClassToken);

        assertEquals("First result should have maximum length", StatsCounter.EVENTS_HISTORY_LENGTH, eventsResult.size());
        assertEquals("Second result should have maximum length", StatsCounter.EVENTS_HISTORY_LENGTH,
                secondEventsResult.size());
        assertEquals("Should log proper number of constructions before last add",
                Integer.valueOf(StatsCounter.EVENTS_HISTORY_LENGTH), constructedResult);
        assertEquals("Should log proper number of constructions after last add",
                Integer.valueOf(StatsCounter.EVENTS_HISTORY_LENGTH + 1), secondConstructedResult);
    }

    private void performConstructionsAndDestructions(final Class<?> clazz, final int constructions,
            final int destructions) {
        for (int i = 0; i < constructions; ++i) {
            testedObject.instanceConstructed(clazz);
        }
        for (int i = 0; i < destructions; ++i) {
            testedObject.instanceDestroyed(clazz);
        }
    }

    private static class TestClass {
    }

    private static class TestClass2 {
    }
}
