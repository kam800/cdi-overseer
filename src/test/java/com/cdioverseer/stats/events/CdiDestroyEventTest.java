/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.events;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CdiDestroyEventTest {
    private static final long TEST_TIMESTAMP = 1243124L;

    private static final Class<TestClass> TEST_CLASS = TestClass.class;

    private static final String TEST_MESSAGE = TEST_TIMESTAMP + " - instance of " + TEST_CLASS + " destroyed";

    private CdiDestroyEvent testedObject;

    @Before
    public void init() {
        testedObject = new CdiDestroyEvent(TEST_TIMESTAMP, TEST_CLASS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailOnNullToken() {
        new CdiDestroyEvent(TEST_TIMESTAMP, null);
    }

    @Test
    public void shouldReturnProperMessage() {
        final String message = testedObject.getMessage();

        assertEquals("Should return proper message", TEST_MESSAGE, message);
    }

    @Test
    public void shouldReturnProperClassToken() {
        final Class<?> classToken = testedObject.getClassToken();

        assertEquals("Should return proper class token", TEST_CLASS, classToken);
    }

    @Test
    public void shouldReturnProperTimestamp() {
        final long timestamp = testedObject.getTimestamp();

        assertEquals("Should return proper timestamp", TEST_TIMESTAMP, timestamp);
    }

    private static class TestClass {
    }
}
