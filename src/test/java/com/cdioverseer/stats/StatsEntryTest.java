/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StatsEntryTest {

    private final String TEST_CLASS_NAME = "class nam";

    private final int TEST_CONSTRUCTED_COUNT = 13;

    private final int TEST_DESTROYED_COUNT = 9;

    @Test(expected = IllegalArgumentException.class)
    public void shouldComplainAboutNullClassName() {
        new StatsEntry(null, TEST_CONSTRUCTED_COUNT, TEST_DESTROYED_COUNT);
    }

    @Test
    public void shouldConstructObject() {
        final StatsEntry result = new StatsEntry(TEST_CLASS_NAME, TEST_CONSTRUCTED_COUNT, TEST_DESTROYED_COUNT);

        assertEquals("Should return valid class name", TEST_CLASS_NAME, result.getClassName());
        assertEquals("Should return valid constructed count", TEST_CONSTRUCTED_COUNT, result.getConstructedCount());
        assertEquals("Should return valid destroyed count", TEST_DESTROYED_COUNT, result.getDestroyedCount());
    }
}
