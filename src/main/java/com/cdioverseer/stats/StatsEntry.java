/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats;

import java.io.Serializable;

public class StatsEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String className;

    private final int constructedCount;

    private final int destroyedCount;

    public StatsEntry(final String className, final int constructedCount, final int destroyedCount) {
        if (className == null) {
            throw new IllegalArgumentException("Null param: className");
        }
        this.className = className;
        this.constructedCount = constructedCount;
        this.destroyedCount = destroyedCount;
    }

    public String getClassName() {
        return className;
    }

    public int getConstructedCount() {
        return constructedCount;
    }

    public int getDestroyedCount() {
        return destroyedCount;
    }
}
