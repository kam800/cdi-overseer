/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.comparator;

import java.util.Comparator;

import com.cdioverseer.stats.StatsEntry;

public class StatsEntryConstructedCountComparator implements Comparator<StatsEntry> {

    @Override
    public int compare(final StatsEntry o1, final StatsEntry o2) {
        return o1.getConstructedCount() - o2.getConstructedCount();
    }

}
