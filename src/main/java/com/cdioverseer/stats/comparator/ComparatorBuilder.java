/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.comparator;

import java.util.Collections;
import java.util.Comparator;

import com.cdioverseer.query.OrderBy;
import com.cdioverseer.query.Ordering;
import com.cdioverseer.query.Query;
import com.cdioverseer.stats.StatsEntry;

public final class ComparatorBuilder {

    private OrderBy orderBy;

    private Ordering ordering;

    private ComparatorBuilder() {
    }

    public ComparatorBuilder withOrderBy(final OrderBy orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public ComparatorBuilder withOrdering(final Ordering ordering) {
        this.ordering = ordering;
        return this;
    }

    public ComparatorBuilder withQuery(final Query query) {
        this.orderBy = query.getOrderBy();
        this.ordering = query.getOrdering();
        return this;
    }

    public static ComparatorBuilder createBuilder() {
        return new ComparatorBuilder();
    }

    public Comparator<StatsEntry> build() {
        final Comparator<StatsEntry> columnComparator = getColumnComparator(orderBy);
        if (ordering == Ordering.ASC) {
            return columnComparator;
        } else if (ordering == Ordering.DESC) {
            return Collections.reverseOrder(columnComparator);
        } else {
            throw new IllegalStateException("Given ordering is not implemented");
        }
    }

    private Comparator<StatsEntry> getColumnComparator(final OrderBy orderBy) {
        if (orderBy == OrderBy.CLASS_NAME) {
            return new StatsEntryClassComparator();
        } else if (orderBy == OrderBy.CONSTRUCTED_COUNT) {
            return new StatsEntryConstructedCountComparator();
        } else if (orderBy == OrderBy.DESTROYED_COUNT) {
            return new StatsEntryDestroyedCountComparator();
        } else {
            throw new IllegalStateException("Given order by not implemented");
        }
    }
}
