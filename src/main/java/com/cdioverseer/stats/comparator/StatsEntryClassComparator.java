/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.comparator;

import java.util.Comparator;

import com.cdioverseer.stats.StatsEntry;

public class StatsEntryClassComparator implements Comparator<StatsEntry> {

    @Override
    public int compare(final StatsEntry o1, final StatsEntry o2) {
        return o1.getClassName().compareTo(o2.getClassName());
    }

}
