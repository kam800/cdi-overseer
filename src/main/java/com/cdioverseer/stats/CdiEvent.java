/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats;

import java.io.Serializable;

public interface CdiEvent extends Serializable {

    /**
     * <p>
     * Returns a timestamp of event occurence.
     * </p>
     * 
     * @return timestamp of occurence
     */
    long getTimestamp();

    /**
     * <p>
     * Returns a class token of instance that caused this event.
     * </p>
     * 
     * @return class of reason instance
     */
    Class<?> getClassToken();

    /**
     * <p>
     * Returns human readable message for this event.
     * </p>
     * 
     * @return human readable message
     */
    String getMessage();
}
