/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.events;

import com.cdioverseer.stats.CdiEvent;

public abstract class AbstractCdiEvent implements CdiEvent {

    private static final long serialVersionUID = 1L;

    private final long timestamp;

    private final Class<?> classToken;

    AbstractCdiEvent(final long timestamp, final Class<?> classToken) {
        if (classToken == null) {
            throw new IllegalArgumentException("Null param: classToken");
        }
        this.timestamp = timestamp;
        this.classToken = classToken;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public Class<?> getClassToken() {
        return classToken;
    }
}
