/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.events;

public class CdiConstructEvent extends AbstractCdiEvent {

    private static final long serialVersionUID = 1L;

    public CdiConstructEvent(final long timestamp, final Class<?> c) {
        super(timestamp, c);
    }

    @Override
    public String getMessage() {
        return String.format("%d - instance of %s constructed", getTimestamp(), getClassToken());
    }
}
