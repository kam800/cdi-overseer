/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.events;

public class CdiDestroyEvent extends AbstractCdiEvent {

    private static final long serialVersionUID = 1L;

    public CdiDestroyEvent(final long timestamp, final Class<?> classToken) {
        super(timestamp, classToken);
    }

    @Override
    public String getMessage() {
        return String.format("%d - instance of %s destroyed", getTimestamp(), getClassToken());
    }
}
