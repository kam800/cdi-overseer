/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.counter;

/**
 * <p>
 * Class is not thread safe.
 * </p>
 */
public class MutableInteger {

    private int value;

    public MutableInteger(final int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(final int value) {
        this.value = value;
    }

    public int incValue() {
        return ++this.value;
    }
}
