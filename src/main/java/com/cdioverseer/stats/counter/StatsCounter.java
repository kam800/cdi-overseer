/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.stats.counter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.cdioverseer.stats.CdiEvent;
import com.cdioverseer.stats.events.CdiConstructEvent;
import com.cdioverseer.stats.events.CdiDestroyEvent;

/**
 * <p>
 * Class is thread safe.
 * </p>
 */
public class StatsCounter {

    public static final int EVENTS_HISTORY_LENGTH = 1000;

    private final Object lock = new Object();

    private final Map<Class<?>, MutableInteger> constructCounts = new HashMap<Class<?>, MutableInteger>();

    private final Map<Class<?>, MutableInteger> destroyCounts = new HashMap<Class<?>, MutableInteger>();

    private final LinkedList<CdiEvent> events = new LinkedList<CdiEvent>();

    public void instanceConstructed(final Class<?> c) {
        synchronized (lock) {
            getNotNullMutableInteger(constructCounts, c).incValue();
            registerEvent(new CdiConstructEvent(System.currentTimeMillis(), c));
        }
    }

    public void instanceDestroyed(final Class<?> c) {
        synchronized (lock) {
            getNotNullMutableInteger(destroyCounts, c).incValue();
            registerEvent(new CdiDestroyEvent(System.currentTimeMillis(), c));
        }
    }

    public Map<Class<?>, Integer> getConstructedCounts() {
        synchronized (lock) {
            return getMutableIntegerMapAsIntegerMap(constructCounts);
        }
    }

    public Map<Class<?>, Integer> getDestroyedCounts() {
        synchronized (lock) {
            return getMutableIntegerMapAsIntegerMap(destroyCounts);
        }
    }

    public List<CdiEvent> getEventList() {
        synchronized (lock) {
            return Collections.unmodifiableList(new ArrayList<CdiEvent>(events));
        }
    }

    public void clear() {
        synchronized (lock) {
            this.constructCounts.clear();
            this.destroyCounts.clear();
            this.events.clear();
        }
    }

    private <X> MutableInteger getNotNullMutableInteger(final Map<X, MutableInteger> map, final X key) {
        MutableInteger mutableInteger = map.get(key);
        if (mutableInteger == null) {
            mutableInteger = new MutableInteger(0);
            map.put(key, mutableInteger);
        }
        return mutableInteger;
    }

    private <X> Map<X, Integer> getMutableIntegerMapAsIntegerMap(final Map<X, MutableInteger> map) {
        final Map<X, Integer> result = new HashMap<X, Integer>();
        for (final Entry<X, MutableInteger> entry : map.entrySet()) {
            result.put(entry.getKey(), entry.getValue().getValue());
        }
        return Collections.unmodifiableMap(result);
    }

    private void registerEvent(final CdiEvent event) {
        events.addLast(event);
        if (events.size() > EVENTS_HISTORY_LENGTH) {
            events.removeFirst();
        }
    }
}
