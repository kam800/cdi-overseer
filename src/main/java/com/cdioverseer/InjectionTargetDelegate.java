/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer;

import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.InjectionTarget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdioverseer.stats.counter.StatsCounter;

class InjectionTargetDelegate<T> implements InjectionTarget<T> {

    private static final Logger LOG = LoggerFactory.getLogger(InjectionTargetDelegate.class);

    private final InjectionTarget<T> injectionTarget;

    private final StatsCounter statsCounter;

    public InjectionTargetDelegate(final InjectionTarget<T> injectionTarget, final StatsCounter statsCounter) {
        if (injectionTarget == null) {
            throw new IllegalArgumentException("Null param: injectionTarget");
        }
        if (statsCounter == null) {
            throw new IllegalArgumentException("Null param: statsCounter");
        }
        this.injectionTarget = injectionTarget;
        this.statsCounter = statsCounter;
    }

    @Override
    public void inject(final T t, final CreationalContext<T> cc) {
        LOG.debug("INJECT into {}", t);
        this.injectionTarget.inject(t, cc);
    }

    @Override
    public void postConstruct(final T t) {
        this.injectionTarget.postConstruct(t);
        LOG.debug("POST CONSTRUCTED {}", t);
        statsCounter.instanceConstructed(t.getClass());
    }

    @Override
    public void preDestroy(final T t) {
        LOG.debug("PRE DESTROYING of {}", t);
        statsCounter.instanceDestroyed(t.getClass());
        this.injectionTarget.preDestroy(t);
    }

    @Override
    public T produce(final CreationalContext<T> cc) {
        final T product = this.injectionTarget.produce(cc);
        LOG.debug("PRODUCED instance {}", product);
        return product;
    }

    @Override
    public void dispose(final T t) {
        LOG.debug("DISPOSING instance {}", t);
        this.injectionTarget.dispose(t);
    }

    @Override
    public Set<InjectionPoint> getInjectionPoints() {
        return this.injectionTarget.getInjectionPoints();
    }
}
