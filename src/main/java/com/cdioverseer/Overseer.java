/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessInjectionTarget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cdioverseer.query.Query;
import com.cdioverseer.stats.StatsEntry;
import com.cdioverseer.stats.comparator.ComparatorBuilder;
import com.cdioverseer.stats.counter.StatsCounter;

public class Overseer implements Extension {

    private static final Logger LOG = LoggerFactory.getLogger(Overseer.class);

    private final StatsCounter statsCounter = new StatsCounter();

    <X> void processInjectionTarget(@Observes final ProcessInjectionTarget<X> processInjectionTarget) {
        LOG.debug("Processing injection target {}", processInjectionTarget.getAnnotatedType());
        final InjectionTargetDelegate<X> it = new InjectionTargetDelegate<X>(
                processInjectionTarget.getInjectionTarget(), statsCounter);
        processInjectionTarget.setInjectionTarget(it);
    }

    /**
     * <p>
     * Clears statistics recorded by Overseer
     * </p>
     */
    public void clearStats() {
        statsCounter.clear();
    }

    /**
     * Queries recorded statistics for specific data.
     * 
     * @param query
     *            specification of data to query for
     * @return list of {@class StatsEntry} instances containing result of query
     */
    public List<StatsEntry> getQueryResult(final Query query) {
        if (query == null) {
            throw new IllegalArgumentException("Null param: query");
        }
        final List<StatsEntry> stats = createAllStats();
        sortStatsWithQuery(stats, query);
        return getSublistWithQuery(stats, query);
    }

    private List<StatsEntry> createAllStats() {
        final Map<Class<?>, Integer> constructedMap = statsCounter.getConstructedCounts();
        final Map<Class<?>, Integer> destroyedMap = statsCounter.getDestroyedCounts();
        final List<StatsEntry> result = new ArrayList<StatsEntry>(constructedMap.size());
        for (final Class<?> clazz : constructedMap.keySet()) {
            final Integer constructedCount = getNumberFromMap(constructedMap, clazz);
            final Integer destroyedCount = getNumberFromMap(destroyedMap, clazz);
            result.add(new StatsEntry(clazz.getName(), constructedCount, destroyedCount));
        }
        return result;
    }

    private Integer getNumberFromMap(final Map<Class<?>, Integer> map, final Class<?> clazz) {
        final Integer constructedCount = map.get(clazz);
        if (constructedCount == null) {
            return Integer.valueOf(0);
        } else {
            return constructedCount;
        }
    }

    private void sortStatsWithQuery(final List<StatsEntry> stats, final Query query) {
        final Comparator<StatsEntry> comparator = ComparatorBuilder.createBuilder().withQuery(query).build();
        Collections.sort(stats, comparator);
    }

    private ArrayList<StatsEntry> getSublistWithQuery(final List<StatsEntry> stats, final Query query) {
        return new ArrayList<StatsEntry>(getSubList(stats, query.getStart(), query.getCount()));
    }

    private List<StatsEntry> getSubList(final List<StatsEntry> stats, final int start, final int count) {
        if (start < 0) {
            throw new IllegalArgumentException("Negative param: start");
        }
        if (count < 0) {
            throw new IllegalArgumentException("Negative param: count");
        }
        final int statsSize = stats.size();
        int begin = start;
        if (begin > statsSize) {
            begin = statsSize;
        }
        int end = start + count;
        if (end > statsSize) {
            end = statsSize;
        }
        return stats.subList(begin, end);
    }
}