/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.query;

public enum OrderBy {

    /**
     * Order by comparing Class.getName() of injected beans
     */
    CLASS_NAME,

    /**
     * Order by number of registered Bean.postContruct() performed
     */
    CONSTRUCTED_COUNT,

    /**
     * Order by number of registered Bean.preDestroy() performed
     */
    DESTROYED_COUNT;

}
