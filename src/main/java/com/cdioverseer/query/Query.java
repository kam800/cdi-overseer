/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.query;

import java.io.Serializable;

//TODO: resolveRead
public final class Query implements Serializable {

    private static final long serialVersionUID = 1L;

    private final OrderBy orderBy;

    private final Ordering ordering;

    private final int start;

    private final int count;

    private Query(final OrderBy orderBy, final Ordering ordering, final int start, final int count) {
        if (orderBy == null) {
            throw new IllegalArgumentException("Null param: orderBy");
        }
        if (ordering == null) {
            throw new IllegalArgumentException("Null param: ordering");
        }
        if (start < 0) {
            throw new IllegalArgumentException("Start less than zero");
        }
        if (count < 0) {
            throw new IllegalArgumentException("Count less than zero");
        }
        this.orderBy = orderBy;
        this.ordering = ordering;
        this.start = start;
        this.count = count;
    }

    public OrderBy getOrderBy() {
        return orderBy;
    }

    public Ordering getOrdering() {
        return ordering;
    }

    public int getStart() {
        return start;
    }

    public int getCount() {
        return count;
    }

    public static Builder createBuilder() {
        return new Builder();
    }

    public static final class Builder {

        private OrderBy orderBy;
        private Ordering ordering;
        private int start;
        private int count;

        private Builder() {
        }

        /**
         * @param orderBy
         *            ordering criteria
         */
        public Builder withOrderBy(final OrderBy orderBy) {
            this.orderBy = orderBy;
            return this;
        }

        /**
         * @param ordering
         *            ordering direction
         */
        public Builder withOrdering(final Ordering ordering) {
            this.ordering = ordering;
            return this;
        }

        /**
         * @param start
         *            index of first entry to query
         */
        public Builder withStart(final int start) {
            this.start = start;
            return this;
        }

        /**
         * @param count
         *            number of entries to query
         */
        public Builder withCount(final int count) {
            this.count = count;
            return this;
        }

        public Query build() {
            return new Query(orderBy, ordering, start, count);
        }
    }
}
