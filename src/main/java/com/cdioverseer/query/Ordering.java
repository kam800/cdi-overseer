/**
 * Copyright (c) 2012, Kamil Borzym
 */
package com.cdioverseer.query;

public enum Ordering {

    /**
     * Order ascending
     */
    ASC,

    /**
     * Order descending
     */
    DESC

}
